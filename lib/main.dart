import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main() {
  runApp(WeatherApp());
}

class MyAppTheme {
  static ThemeData appTheme() {
    return ThemeData(
      brightness: Brightness.dark,
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    );
  }
}

class WeatherApp extends StatefulWidget {
  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: MyAppTheme.appTheme(),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.square(58),
            child: AppBar(
                flexibleSpace: Container(
                    decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                        "https://i.pinimg.com/564x/97/f5/c4/97f5c43caebf5b3999d598bc05199eb0.jpg"),
                    fit: BoxFit.cover,
                  ),
                )),
                // backgroundColor: Colors.white,
                elevation: 0,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Seoul",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 45,
                          fontWeight: FontWeight.w300),
                    ),
                  ],
                )),
          ),
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    "https://i.pinimg.com/564x/97/f5/c4/97f5c43caebf5b3999d598bc05199eb0.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: ListView(
              children: <Widget>[
                Weather(),
                AllDay(),
                Weekly(),
              ],
            ),
          ),
        ));
  }
}

Widget AllDay() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.grey.withOpacity(0),
    child: SizedBox(
      height: 150,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          timeNow(),
          timeSunrise(),
          timeSkip(),
          timeSkip2(),
          timeSkip3(),
          timeSkip4(),
          timeSkip5(),
          timeSkip6(),
          timeSkip7(),
          timeSkip8(),
          timeSkip9(),
          timeSkip10(),
          timeSkip11(),
          timeSunset(),
        ],
      ),
    ),
  );
}

Widget Weather() {
  return Column(
    children: <Widget>[
      Text(
        "-7°",
        style: TextStyle(fontSize: 100, height: 1.5, color: Colors.white),
      ),
      Text(
        "Mostly Clear",
        style: TextStyle(fontSize: 20, color: Colors.white70),
      ),
      Text(
        "H:1°  L:-8°",
        style: TextStyle(fontSize: 20, height: 2, color: Colors.white70),
      )
    ],
  );
}

Widget timeNow() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "Now",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.moon_stars_fill,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "-7°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSunrise() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "07:47",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sunrise_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "Sunrise",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "08",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "-7°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip2() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "09",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "-6°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip3() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "09",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "-4°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip4() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "10",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "-4°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip5() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "11",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "-2°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip6() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "12",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "-1°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip7() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "13",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "0°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip8() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "14",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "1°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip9() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "15",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "1°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip10() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "16",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "1°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSkip11() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "17",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sun_max_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "0°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}

Widget timeSunset() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "17:26",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Icon(
          CupertinoIcons.sunset_fill,
          color: Colors.yellow,
          size: 30,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "Sunset",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],
  );
}
Widget textWeek() {
  return Row(
    children: <Widget>[
      Icon(
        CupertinoIcons.calendar,
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 20,
          bottom: 20,
          left: 5,
        ),
        child: Text(
          "10-DAY FORECAST",
          style: TextStyle(fontSize: 20,color: Colors.white54),
        ),
      ),
    ],
  );
}

Widget Day() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Today",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
          Text(
            "Thu",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "Fri",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "Sat",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "Sun",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "Mon",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "Tue",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "Wed",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
          Text(
            "Thu",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
          Text(
            "Fri",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
        ],
      )
    ],
  );
}

Widget IconDay() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(
            CupertinoIcons.sun_max_fill,
            color: Colors.yellow,
            size: 40,
          ),
          Icon(
            CupertinoIcons.sun_max_fill,
            color: Colors.yellow,
            size: 40,
          ),
          Icon(
            CupertinoIcons.snow,
            size: 40,
          ),
          Icon(
            CupertinoIcons.cloud_sun_fill,
            size: 40,
          ),
          Icon(
            CupertinoIcons.sun_max_fill,
            color: Colors.yellow,
            size: 40,
          ),
          Icon(
            CupertinoIcons.cloud_sun_fill,
            size: 40,
          ),
          Icon(
            CupertinoIcons.sun_max_fill,
            color: Colors.yellow,
            size: 40,
          ),
          Icon(
            CupertinoIcons.sun_max_fill,
            color: Colors.yellow,
            size: 40,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 40,
          ),
          Icon(
            CupertinoIcons.cloud_drizzle_fill,
            size: 40,
          ),
        ],
      )
    ],
  );
}

Widget temp() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "-8° ----- 1°",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
          Text(
            "-8° ----- 3°",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "-5° ----- 4°",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "-3° ----- 3°",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "-5° ----- 4°",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "-2° ----- 4°",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "-3° ----- 5°",
            style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
          ),
          Text(
            "-3° ----- 7°",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
          Text(
            "-3° ----- 9°",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
          Text(
            "1° ----- 11°",
            style: TextStyle(fontSize: 20,color: Colors.white),
          ),
        ],
      )
    ],
  );
}

Widget Weekly() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white.withOpacity(0.2),
    child: SizedBox(
      height: 450,
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Column(
            children: <Widget>[
              textWeek(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Day(),
                  IconDay(),
                  temp(),
                ],
              )
            ],
          )
        ],
      ),
    ),
  );
}
